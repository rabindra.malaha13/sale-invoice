﻿using Microsoft.EntityFrameworkCore;
using SalesInvoice.Data;
using SalesInvoice.Data.ApplicationDbContext;
using SalesInvoice.Data.Autogenerates;
using SalesInvoice.Data.Datas;
using SalesInvoice.Data.Mappings;
using SalesInvoice.Domain;
using SalesInvoice.Domain.AutoGenerates;
using SalesInvoice.Domain.Datas;
using SalesInvoice.Domain.Mappings;
using SalesInvoice.ServiceImplementation.AppInputs;
using SalesInvoice.ServiceInterface.AppInputs;

namespace SaleInvoice.Api.Host.Configurations
{
    public static class SaleInvoiceConfigurations
    {
        public static void ConfigureAppServices(WebApplicationBuilder builder)
        {
            builder.Services.AddDbContext<SaleInvoiceDbContext>(options =>
            {
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            builder.Services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            builder.Services.AddScoped<IAutogenerateRepository, AutogenerateRepository>();
            builder.Services.AddScoped<IDataRepository, DataRepository>();
            builder.Services.AddScoped<IMappingRepository, MappingRepository>();

            builder.Services.AddScoped<IInputService, InputService>();

        }
    }
}
