﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesInvoice.ServiceInterface.AppInputs;

namespace SaleInvoice.Api.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyAppController : ControllerBase, IInputService
    {
        private readonly IInputService _service;
        public MyAppController(IInputService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<InputDto> Create(InputDto input)
        {
            return await _service.Create(input);
        }

        [HttpGet("{typeId}")]
        public async Task<OutputDto> GetOutput(int typeId)
        {
            return await _service.GetOutput(typeId);
        }
    }
}
