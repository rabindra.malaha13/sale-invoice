﻿using Microsoft.EntityFrameworkCore.Storage;
using SalesInvoice.Data.ApplicationDbContext;
using SalesInvoice.Domain.AutoGenerates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.Data.Autogenerates
{
    public class AutogenerateRepository : Repository<AutoGenerate>, IAutogenerateRepository
    {
        public AutogenerateRepository(SaleInvoiceDbContext context) : base(context)
        {
        }
    }
}
