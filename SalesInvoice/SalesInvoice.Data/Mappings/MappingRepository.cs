﻿using SalesInvoice.Data.ApplicationDbContext;
using SalesInvoice.Domain.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.Data.Mappings
{
    public class MappingRepository : Repository<Mapping>, IMappingRepository
    {
        public MappingRepository(SaleInvoiceDbContext context) : base(context)
        {
        }
    }
}
