﻿using SalesInvoice.Data.ApplicationDbContext;
using SalesInvoice.Domain.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.Data.Datas
{
    public class DataRepository : Repository<SalesInvoice.Domain.Datas.Data>, IDataRepository
    {
        public DataRepository(SaleInvoiceDbContext context) : base(context)
        {
        }
    }
}
