﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SalesInvoice.Domain.AutoGenerates;
using SalesInvoice.Domain.Datas;
using SalesInvoice.Domain.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SalesInvoice.Data.ApplicationDbContext
{
    public class SaleInvoiceDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public SaleInvoiceDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("SaleInvoiceConnection"));
        }

        public virtual DbSet<AutoGenerate> AutoGenerates { get; set; }
        public virtual DbSet<SalesInvoice.Domain.Datas.Data> Datas { get; set; }
        public virtual DbSet<Mapping> Mappings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)// Crée la migration
        {
            modelBuilder.Entity<SalesInvoice.Domain.Datas.Data>(b =>
            {
                b.ToTable("tbl_Data");
                b.HasOne<AutoGenerate>().WithMany().HasForeignKey(x => x.Id).IsRequired();
            });

            modelBuilder.Entity<Mapping>(b =>
            {
                b.ToTable("tbl_Mappings");
                //b.HasOne<SalesInvoice.Domain.Datas.Data>().WithMany().HasForeignKey(x => x.Key).IsRequired();
                //b.HasOne<SalesInvoice.Domain.Datas.Data>().WithMany().HasForeignKey(x => x.Value).IsRequired();
                //b.HasOne<SalesInvoice.Domain.Datas.Data>().WithMany().HasForeignKey(x => x.TypeId).IsRequired();
            });

            modelBuilder.Entity<AutoGenerate>(b =>
            {
                b.ToTable("tbl_Autogenerate");
                b.Property(x => x.Id).ValueGeneratedNever();
            });
        }
    }
}
