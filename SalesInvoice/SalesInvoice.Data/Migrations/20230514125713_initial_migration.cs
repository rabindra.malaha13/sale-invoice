﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SalesInvoice.Data.Migrations
{
    /// <inheritdoc />
    public partial class initial_migration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbl_Autogenerate",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_Autogenerate", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbl_Mappings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<int>(type: "int", nullable: false),
                    TypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_Mappings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbl_Data",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    DataName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_Data", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tbl_Data_tbl_Autogenerate_Id",
                        column: x => x.Id,
                        principalTable: "tbl_Autogenerate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbl_Data");

            migrationBuilder.DropTable(
                name: "tbl_Mappings");

            migrationBuilder.DropTable(
                name: "tbl_Autogenerate");
        }
    }
}
