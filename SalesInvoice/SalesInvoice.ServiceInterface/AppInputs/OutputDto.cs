﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.ServiceInterface.AppInputs
{
    public class OutputDto
    {
        public SaleInvoiceDto SaleInvoice { get; set; }
    }
}
