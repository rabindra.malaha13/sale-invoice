﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.ServiceInterface.AppInputs
{
    public interface IInputService
    {
        Task<InputDto> Create(InputDto input);
        Task<OutputDto> GetOutput(int typeId);
    }
}
