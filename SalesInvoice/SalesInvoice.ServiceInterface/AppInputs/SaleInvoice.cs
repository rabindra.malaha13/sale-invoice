﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.ServiceInterface.AppInputs
{
    public class SaleInvoiceDto
    {
        public string? ItemName { get; set; }
        public string? Quantity { get; set; }
        public string? Rate { get; set; }
        public string? Total { get; set; }
    }
}
