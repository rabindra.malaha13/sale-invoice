﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.Domain.Mappings
{
    public class Mapping : BaseEntity
    {
        public int Key { get; set; }
        public int Value { get; set; }
        public int TypeId { get; set; }
    }
}
