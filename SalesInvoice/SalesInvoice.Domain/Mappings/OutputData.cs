﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.Domain.Mappings
{
    public class OutputData
    {
        public SaleInvoice SaleInvoice { get; set; }
    }

    public class SaleInvoice
    {
        public string? ItemName { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal Total { get; set; }
    }
}
