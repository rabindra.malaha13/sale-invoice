﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.Domain.Mappings
{
    public interface IMappingRepository : IRepository<Mapping>
    {
    }
}
