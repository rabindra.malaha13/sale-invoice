﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.Domain.Datas
{
    //[Keyless]
    public class Data
    {
        //Name can't be same as class name.
        public int Id { get; set; }
        public string? DataName { get; set; }
    }
}
