﻿using Microsoft.Data.SqlClient;
using SalesInvoice.Domain.AutoGenerates;
using SalesInvoice.Domain.Datas;
using SalesInvoice.Domain.Mappings;
using SalesInvoice.ServiceInterface.AppInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SalesInvoice.ServiceImplementation.AppInputs
{
    public class InputService : IInputService
    {
        private readonly IAutogenerateRepository _autogenerateRepository;
        private readonly IDataRepository _dataRepository;
        private readonly IMappingRepository _mappingRepository;

        public InputService(IAutogenerateRepository autogenerateRepository, IDataRepository dataRepository, IMappingRepository mappingRepository)
        {
            _autogenerateRepository = autogenerateRepository;
            _dataRepository = dataRepository;
            _mappingRepository = mappingRepository;
        }

        public async Task<InputDto> Create(InputDto input)
        {
            var SalesInvoice = input.SaleInvoice;
            var datas = new List<SalesInvoice.Domain.Datas.Data>();
            int autoId = 1;
            var autoes = await _autogenerateRepository.GetListAsync();
            if (autoes.Any())
            {
                autoId = autoes.Count() + 1;
            }
            int typeId = autoId;

            datas.Add(new Domain.Datas.Data { DataName = "SalesInvoice" });

            var keyValuePairs = new Dictionary<string, string>()
            {
                {"ItemName", SalesInvoice.ItemName },
                {"Quantity", SalesInvoice.Quantity },
                {"Rate", SalesInvoice.Rate },
                {"Total", SalesInvoice.Total }
            };

            var mappings = new List<Mapping>();

            foreach (var item in keyValuePairs)
            {
                datas.Add(new Domain.Datas.Data { DataName = item.Key });
                datas.Add(new Domain.Datas.Data { DataName = item.Value });
            }

            foreach (var item in datas)
            {
                var auto = await _autogenerateRepository.InsertAsync(new AutoGenerate { Id = autoId });
                item.Id = autoId;
                autoId += 1;
            }

            var insertedDatas = await _dataRepository.InsertManyAsync(datas);

            var datasDict = insertedDatas.ToDictionary(x => x.Id, x => x.DataName);

            foreach (var item in keyValuePairs)
            {
                var key = datasDict.FirstOrDefault(x => x.Value == item.Key).Key;
                var value = datasDict.FirstOrDefault(x => x.Value == item.Value).Key;
                var mapping = new Mapping
                {
                    Key = key,
                    Value = value,
                    TypeId = typeId
                };

                mappings.Add(mapping);
            }

            await _mappingRepository.InsertManyAsync(mappings);

            return input;
        }

        public async Task<OutputDto> GetOutput(int typeId)
        {
            var dictionaryData = await GetOuput(typeId);

            var saleInvoice = new SaleInvoiceDto
            {
                ItemName = dictionaryData["ItemName"],
                Quantity = dictionaryData["Quantity"],
                Rate = dictionaryData["Rate"],
                Total = dictionaryData["Total"]
            };
            return new OutputDto
            {
                SaleInvoice = saleInvoice
            };
        }

        private async Task<Dictionary<string, string>> GetOuput(int typeId)
        {
            var mappings = await _mappingRepository.GetListAsync();
            var datas = await _dataRepository.GetListAsync();

            var mappingQuery = from mapping in mappings
                               join keydata in datas on mapping.Key equals keydata.Id into keyDatas
                               from keydata in keyDatas.DefaultIfEmpty()
                               join valuedata in datas on mapping.Value equals valuedata.Id into valueDatas
                               from valuedata in valueDatas.DefaultIfEmpty()
                               where mapping.TypeId == typeId
                               select new KeyValuePair<string, string>
                               (
                                    keydata.DataName!,
                                    valuedata.DataName!
                               );

            return mappingQuery.ToDictionary(x => x.Key, x => x.Value);
        }
    }
}
